package org.dikhim.jclicker.actions.actions;

import java.awt.*;

public interface Action {
    ActionType getType();
}
