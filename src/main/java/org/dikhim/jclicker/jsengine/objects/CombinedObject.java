package org.dikhim.jclicker.jsengine.objects;

public interface CombinedObject {
    void run(String encoding, String code);
}
