package org.dikhim.jclicker.jsengine.objects;

@SuppressWarnings("unused")
public interface ClipboardObject {
    String get();

    void set(String str);
}
